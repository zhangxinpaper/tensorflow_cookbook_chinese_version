# 数据源信息

这里是本书要用到的数据源。下面的链接是带注释和引用的原始数据源。本目录中的脚本介绍了如何获取这些数据源

 1. [Iris Data](http://scikit-learn.org/stable/auto_examples/datasets/plot_iris_dataset.html)
 2. [Low Birthweight Data](https://github.com/nfmcclure/tensorflow_cookbook/raw/master/01_Introduction/07_Working_with_Data_Sources/birthweight_data/birthweight.dat)
 3. [Housing Price Data](https://archive.ics.uci.edu/ml/datasets/Housing)
 4. [MNIST Dataset of Handwritten Digits](http://yann.lecun.com/exdb/mnist/)
 5. [SMS Spam Data](https://archive.ics.uci.edu/ml/datasets/SMS+Spam+Collection)
 6. [Movie Review Data](http://www.cs.cornell.edu/people/pabo/movie-review-data/)
 7. [William Shakespeare Data](http://www.gutenberg.org/ebooks/100)
 8. [German-English Sentence Data](http://www.manythings.org/anki/)


