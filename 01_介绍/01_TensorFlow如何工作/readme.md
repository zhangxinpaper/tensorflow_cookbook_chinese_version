# 介绍Tensorflow如何工作

要看更多的细节，可以看[Jupyter Notebook](01_How_TensorFlow_Works.ipynb).

TensorFlow有一个中独一无二的解决问题的方法。该独一无二的方法解决机器学习问题非常有效。有一些解决大部分Tensorflow算法的通用步骤：

  1. 载入数据、生成数据、或者用placeholders设置一个数据管道
  2. 通过计算图传入数据
  3. 在损失函数上评估输出
  4. 使用反向传播更新变量
  5. 重复上述步骤直到满足结束条件

![Computational Graph](../images/01_outline.png "A general outline of computational graphs")

对于本书中覆盖许多算法，现在有更多的细微描述。我们将讨论转换数据、归一化数据、使用变量、创建placeholder、初始化对象、定义计算图结构、创建损失函数、训练模型和评估结果。