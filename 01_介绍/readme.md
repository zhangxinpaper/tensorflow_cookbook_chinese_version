## Ch 1: TensorFlow开始


本章介绍Tensorflow的主要对象和概念。我们为本书剩余章节介绍如何访问数据，以及提供TensorFlow的其他学习资源。

1. TF算法一般步骤
   - 这里介绍TensorFlow和大多数TensorFlow算法所遵循的步骤
2. 创建和使用张量
   - 在TensorFlow中如何创建和初始化张量。我们也介绍在Tensorboard中如何操作
3. 使用变量和占位符
   - 在TensorFlow中如何创建和使用变量、占位符。我们也介绍在Tensorboard中如何操作
4. 使用矩阵
   - 理解TensorFlow是如何处理矩阵的，以及这对于理解算法如何工作是非常关键的
5. 声明操作
   - 如何使用TensorFlow的多种多样的数学运算
6. 实现激活函数
   - 激活函数是独一无二的。TensorFlow内建了许多激活函数方便用户使用
7. 使用数据源
   - 这里介绍了本书需要访问的多种数据源。同时也介绍了这些数据来自哪里
8. 其它资源
   - 大多数的官方资源和文章。这些文章都是TensorFlow文件或者深度学习资源


