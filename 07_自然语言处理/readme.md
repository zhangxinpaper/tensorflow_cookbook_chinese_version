## Ch 7: 自然语言处理

自然语言处理（NLP）是一种处理文本信息为数值特征或者模型的方法。在本章中我们将解释如何在TensorFlow中最有处理文本。我们展示如何实现经典的词包，并且展示有更好的方法来嵌入文本（基于手头的问题）。有一种神经网络嵌入方法称为Word2Vec（CBOW和Skip-Gram）和Doc2Vec。我们展示如何在TensorFlow中实现所有这些功能。

1. 介绍
   - 我们介绍将文本转化为数值向量的方法。我们介绍Tensorflow的嵌入特征。
2. 实现词包
   - 这里我们使用TensorFlow来实现词的独热编码（成为词包）。我们使用这个方法和逻辑回归来预测一个文本信息是spam或者是ham。
3. 实现TF-IDF
   - 我们使用Sci-kit Learn和TensorFlow，来实现文本频率-逆文本频率（TF-IDF，Text Frequency-Inverse Document Frequency）。我们在TF-IDF向量上使用逻辑回归来提高我们的spam/ham文本信息预测器。
4. 实现Skip-Gram
   - Word2Vec的首次实现，称为在电影评论数据库中的skip-gram。
5. 实现CBOW
   - 这不，我们实现Word2Vec的另一个形式，称为在电影评论数据库中的CBOW。我们也介绍方法来存储和加载词嵌入。
6. 实现Word2Vec例子
   - 在本例中，我们使用先前保存的CBOW词嵌入，来提高我们的电影评论情感的TF-IDF逻辑回归。
7. 使用Doc2Vec实现情感分析
   - 这里，我们介绍一个Doc2Vec方法，来提高电影评论情感的逻辑回归模型的效果