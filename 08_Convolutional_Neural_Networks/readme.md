## Ch 8: 卷积神经网络

<kbd>
  <a href="08_Convolutional_Neural_Networks/01_Intro_to_CNN#introduction-to-convolutional-neural-networks">
    <img src="08_Convolutional_Neural_Networks/images/01_intro_cnn.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="08_Convolutional_Neural_Networks/02_Intro_to_CNN_MNIST#introduction-to-cnn-with-mnist">
    <img src="08_Convolutional_Neural_Networks/images/02_cnn1_mnist_output.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="08_Convolutional_Neural_Networks/03_CNN_CIFAR10#cifar-10-cnn">
    <img src="08_Convolutional_Neural_Networks/images/03_cnn2_loss_acc.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="08_Convolutional_Neural_Networks/04_Retraining_Current_Architectures#retraining-fine-tuning-current-cnn-architectures">
    <img src="08_Convolutional_Neural_Networks/images/image.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="08_Convolutional_Neural_Networks/05_Stylenet_NeuralStyle#stylenet--neural-style">
    <img src="08_Convolutional_Neural_Networks/images/05_stylenet_ex.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="08_Convolutional_Neural_Networks/06_Deepdream#deepdream-in-tensorflow">
    <img src="08_Convolutional_Neural_Networks/images/06_deepdream_ex.png" align="center" height="45" width="90">
  </a>
</kbd>

卷积神经网络（CNN）是处理图像数据的常用神经网络。CNN的名字来源于其使用的卷积层（该层在大尺寸图像上使用固定尺寸的滤波器来计算，在图像的任意部分识别一种模式）。有许多其他的工具（最大池化、随机失活等等），并展示如何在Tensorflow中实现。我们也展示如何重新训练一个现有的架构，并且用Stylenet和Deep Dream来使用CNN。

1. 介绍
   - 我们介绍卷及神经网络（CNN），以及在Tensorflow中如何使用他们。
2. 实现一个简单的CNN
   - 这里，我们展示如何创建一个CNN架构（在MNIST数字识别任务中效果良好）。
3. 实现一个先进的CNN
   - 在本例中，我们展示如何复制一个架构，来实现CIFAR-10图像识别任务。
4. 重训练一个现存的框架
   - 我们展示如何下载和安装CIFAR-10数据集，来用于演示TensorFlow的重训练和微调。
5. 使用Stylenet/NeuralStyle
   - 在本拷贝中，我们使用Stylenet或者Neuralstyle展示一个基本的实现。
6. 实现Deep Dream
   - 该脚本对Tensorflow的deepdream教程进行了一行一行的解释。这里代码已经转化为python3