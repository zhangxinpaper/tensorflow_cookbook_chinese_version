# 核技巧

线性SVM的功能非常强。但是有时数据并不是线性的。对于这种情况，我们需要使用“核技巧”将我们的数据映射到更高维的空间，在该空间中数据是线性可分的。使用核技巧使得我们可以分类我们的非线性类别，看如下例子。

如果我们尝试分割以下的圆环形状的数据（使用标准SVM），将不能成功分类。

![线性SVM分类线性不可分数据](../images/04_nonlinear_data_linear_kernel.png "Linear SVM Fit")

但是如果我们使用高斯-径向基核，我们可以在更高维找到线性分割面，在这种情况下，我们可以实现比较准确的分类

![Gaussian Kernel Nonlinear Data](../images/04_linear_svm_gaussian.png "Gaussian Kernel")
