## Ch 4: 支持向量机

<kbd>
  <a href="04_Support_Vector_Machines/01_Introduction#support-vector-machine-introduction">
    <img src="04_Support_Vector_Machines/images/01_introduction.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="04_Support_Vector_Machines/02_Working_with_Linear_SVMs#working-with-linear-svms">
    <img src="04_Support_Vector_Machines/images/02_linear_svm_output.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="04_Support_Vector_Machines/03_Reduction_to_Linear_Regression#svm-reduction-to-linear-regression">
    <img src="04_Support_Vector_Machines/images/03_svm_regression_output.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="04_Support_Vector_Machines/04_Working_with_Kernels#working-with-kernels">
    <img src="04_Support_Vector_Machines/images/04_linear_svm_gaussian.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="04_Support_Vector_Machines/05_Implementing_Nonlinear_SVMs#implementing-nonlinear-svms">
    <img src="04_Support_Vector_Machines/images/05_non_linear_svms.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="04_Support_Vector_Machines/06_Implementing_Multiclass_SVMs#implementing-multiclass-svms">
    <img src="04_Support_Vector_Machines/images/06_multiclass_svm.png" align="center" height="45" width="90">
  </a>
</kbd>

本章使用Tensorflow来实现支持向量机。我们首先创建一个线性SVM，并且也展示如何用于回归。然后我们介绍核方法（RBF高斯核），并且使用它来分割非线性数据集。我们最后使用非线性SVM的多维实现来处理多个类别的数据。


  1. 介绍

      - 我们介绍SVM，并且介绍我们在Tensorflow框架下如何实现他们。
  2. 实现线性SVM

      - 在iris数据中，我们基于花瓣的长度和花蕾的宽度，来创建线性SVM，区分I.setosa。
  3. 降为线性回归

      - SVM的核心是实现线性分类。我们稍微修改算法，来实现SVM回归。
  4. 在Tensorflow中使用核技巧

      - 为了在非线性数据中扩展SVM，我们解释和展现如何在Tensorflow中实现不同的核技巧。
  5. 实现非线性SVM

      - 我们使用高斯核（RBF）来分辨非线性类。
  6. 实现多类SVM

      - SVM是先天的二分类器。我们在Tensorflow中用一对多策略来扩展SVM。