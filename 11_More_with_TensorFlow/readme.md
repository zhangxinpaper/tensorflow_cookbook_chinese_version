## Ch 11: 关于TensorFlow的更多用法

<kbd>
  <a href="11_More_with_TensorFlow/01_Visualizing_Computational_Graphs#visualizing-computational-graphs-wtensorboard">
    <img src="11_More_with_TensorFlow/images/01_tensorboard1.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="11_More_with_TensorFlow/02_Working_with_a_Genetic_Algorithm">
    <img src="11_More_with_TensorFlow/images/02_genetic_algorithm.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="11_More_with_TensorFlow/03_Clustering_Using_KMeans#clustering-using-k-means">
    <img src="11_More_with_TensorFlow/images/03_kmeans.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="11_More_with_TensorFlow/04_Solving_A_System_of_ODEs#solving-a-system-of-odes">
    <img src="11_More_with_TensorFlow/images/04_ode_system.png" align="center" height="45" width="90">
  </a>
</kbd>

为了描述TensorFlow的多功能。我们在本章中介绍一些额外的例子。我们首先介绍用于记录/可视化的工具-Tensorboard。然后我们描述如何实现k均值聚类（使用一个通用算法，并且求解ODEs系统）。

1. 可视化计算图（使用Tensorboard）
   - 在Tensorboard中使用直方图、scalar summaries和创建图像。
2. 实现一个通用算法
   - 我们创建一个通用算法，使用ground truth函数来优化一个个体（50个数的列表）
3. k均值聚类
   - 如何用TensorFlow实现k均值聚类。我们使用iris数据集，令k=3、并且使用k均值来做预测。
4. 求解ODEs系统
   - 这里，我们展示如何使用TensorFlow来求解一个ODEs系统。系统的核心是Lotka-Volterra predator-prey系统。