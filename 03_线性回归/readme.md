## Ch 3: 逻辑回归

<kbd>
  <a href="03_Linear_Regression/01_Using_the_Matrix_Inverse_Method#using-the-matrix-inverse-method">
    <img src="03_Linear_Regression/images/01_Inverse_Matrix_Method.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="03_Linear_Regression/02_Implementing_a_Decomposition_Method#using-the-cholesky-decomposition-method">
    <img src="03_Linear_Regression/images/02_Cholesky_Decomposition.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="03_Linear_Regression/03_TensorFlow_Way_of_Linear_Regression#learning-the-tensorflow-way-of-regression">
    <img src="03_Linear_Regression/images/03_lin_reg_fit.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="03_Linear_Regression/04_Loss_Functions_in_Linear_Regressions#loss-functions-in-linear-regression">
    <img src="03_Linear_Regression/images/04_L1_L2_learningrates.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="03_Linear_Regression/05_Implementing_Deming_Regression#implementing-deming-regression">
    <img src="03_Linear_Regression/images/05_demming_vs_linear_reg.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="03_Linear_Regression/06_Implementing_Lasso_and_Ridge_Regression#implementing-lasso-and-ridge-regression">
    <img src="03_Linear_Regression/images/07_elasticnet_reg_loss.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="03_Linear_Regression/07_Implementing_Elasticnet_Regression#implementing-elasticnet-regression">
    <img src="03_Linear_Regression/images/07_elasticnet_reg_loss.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="03_Linear_Regression/08_Implementing_Logistic_Regression#implementing-logistic-regression">
    <img src="03_Linear_Regression/images/08_logistic_reg_acc.png" align="center" height="45" width="90">
  </a>
</kbd>

这里我们在TensorFlow中实现多种线性回归模型。前两节，使用TensorFlow实现标准的矩阵线性回归。剩下的6节在TensorFlow中使用计算图实现多种回归模型。

1. 使用矩阵求逆方法
   - 在TensorFlow中使用矩阵求逆求解2D回归
2. 实现一个分解方法
   - 使用Cholesky分解求解2D线性回归
3. 学习线性回归的TensorFlow方法
   - 采用L2损失，通过计算图来实现线性回归迭代运算
4. 在线性回归中理解损失函数
   - 线性回归的L2和L1损失。我们研究它们的优缺点
5. 实现Deming回归（全回归）
   - 通过改变损失函数在TensorFlow中实现Deming（全）回归。
6. 实现Lasso和Ridge回归
   - Lasso和Ridge回归是正则化稀疏的的方法。通过在Tensorflow中改变损失函数来实现这两种回归。
7. 实现弹性网络回归
   - 弹性网络是一个正则化方法，用来使用权值来组合L2和L1损失。我们在Tensorflow中实现。
8. 实现逻辑回归
   - 在计算图中使用激活函数来实现逻辑回归