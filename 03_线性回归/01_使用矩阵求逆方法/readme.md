# 使用矩阵求逆方法

这里我们在TensorFlow中基于矩阵求逆方法来解决2D线性回归问题。

# 模型

考虑A * x = b, 我们可以用如下公式求解x:

(t(A) * A) * x = t(A) * b

x = (t(A) * A)^(-1) * t(A) * b

这里我们注意t(A)是A的转置矩阵

# 用于线性拟合的计算图

![Matrix Inverse Method](../images/01_Inverse_Matrix_Method.png "Matrix Inverse Method")
