# 实现Deming回归（新的损失函数）

Deming回归，也被认为是完全回归，是一种通用回归方法（其最小化与线的最短距离）。对比常见的回归问题，其是最小化模型输出和实际y值的距离。

![Deming Regression](../images/05_demming_vs_linear_reg.png "Deming Regression")

# 模型

模型与常规线性回归问题是一样的：

y = A * x + b

与L2距离不同，我们在损失函数中需要计算线与预测点之间的最短距离。

loss = |y\_target - (A * x\_input + b)| / sqrt(A^2 + 1)

# 线性拟合的计算图

![Deming Output](../images/05_demming_reg.png "Deming Output")
