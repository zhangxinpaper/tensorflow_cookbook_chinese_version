# 实现逻辑回归

逻辑回归是一种预测一个数值是0还是1的方法（通常我们通过观察输出概率得到判断结果）。如果预测值大于某个提前设定的值，那么预测分类结果为1，否则为0。标准的切割点设为0.5。对于本例而言，我们指定cutoff为0.5，这样便于实现。

数据使用UMASS低生育率数据集。

[UMASS low birth weight data](https://www.umass.edu/statdata/statdata/data/lowbwt.txt).

# 模型

模型的输出是标准的逻辑回归（在线性模型的基础上加上了sigmoid函数）：

y = sigmoid(A * x + b)

这里的x是矩阵，第一维是批尺寸维度。y目标输出的维度是（批尺寸， 1）。

这里我们使用交叉熵损失的均值：

loss = mean( - y * log(predicted) + (1-y) * log(1-predicted) )

TensorFlow内建交叉熵损失的计算函数，我们使用函数'tf.nn.sigmoid\_cross\_entropy\_with\_logits()'

然后使用皮训练方法来优化。

# 损失函数的计算图

运行脚本可以得到与下面两图相似的损失和精确度曲线。

![Logistic Regression Loss](../images/08_logistic_reg_loss.png "Logistic Regression Loss")

# Accuracy of Train and Test Sets

![Logistic Regression Accuracy](../images/08_logistic_reg_acc.png "Logistic Regression Accuracy")
