# 使用Cholesky分解方法

这里我们在TensorFlow中基于Cholesky分解方法来解决2D线性回归问题。

# 模型

给定A * x = b，并且一个Cholesky分解例如 A = L * L'，然后我们可以使用x来处理

 1. 求解 L * y = t(A) * b for y
 2. 求解 L' * x = y for x.

# 线性拟合的计算图

![Cholesky decomposition](../images/02_Cholesky_Decomposition.png "Cholesky decomposition")
