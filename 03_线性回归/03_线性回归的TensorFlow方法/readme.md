# 学习解决回归问题的TensorFLow方法

在本节中，我们在TensorFlow中将线性回归实现为一个迭代计算图。这里不使用自己产生的数据，我们使用Iris数据集合。我们的x是petal宽度，y是sepal长度（做数据分析的时候这个要看下）。可以通过二维可视化数据，认为它们具有线性关系。

# 模型

模型的输出是一个2D线性回归：

y = A * x + b

x矩阵输入是一个2D矩阵，其维度为（批尺寸 x 1）。y目标输出有相同的维度（批尺寸 x 1）。

损失函数使用L2损失的均值：

loss = mean( (y\_target - model\_output)^2 )

然后使用批量训练模型。

# 损失的计算图

![Regression Loss](../images/03_lin_reg_loss.png "Regression Loss")

# 线性拟合的计算图

![TF Regression](../images/02_Cholesky_Decomposition.png "TF Regression")
