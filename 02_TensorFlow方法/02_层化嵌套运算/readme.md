# 一个计算图中的多个计算操作

## 总结

在本脚本中，我们创建一个列表，并执行两个乘法，如下所示：

output = (input) * (m1) * (m2) + (a1)

## 计算图输出

在Tensorboard中观察计算图

![Multiple Operations](../images/02_Multiple_Operations.png "Multiple Operations on a Graph")