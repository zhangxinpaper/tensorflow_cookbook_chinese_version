# 将前面的知识点综合起来

## 总结

我们在本例中创建一个简单的线性分类器。我们使用Iris数据集。我们优化一个线性分类器。特征是花瓣的长度和宽度来分辨花是_I.setosa_或者不是。我们用这个数据，因为它服从线性可分特性。

## 提示

我们使用sigmoid交叉熵损失函数，因为我们预测一个二分类结果（sigmoid），并且它是一个分类问题（交叉熵）。

## 结果

观察结果图和分类线

![Isetosa](../images/07_Combing_Everything_Together.png "I setosa Seperability")