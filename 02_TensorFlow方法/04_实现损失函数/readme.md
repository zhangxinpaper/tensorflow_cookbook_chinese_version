# 实现损失函数

## 总结

在本脚本中，我们将在tensorflow中实现不同的损失函数。

## 画出损失函数

本节中的脚本输出，将画出多种损失函数

![Loss Functions 1](../images/04_loss_fun1.png "Loss Functions 1")

![Loss Functions 2](../images/04_loss_fun2.png "Loss Functions 2")