# 实现批量梯度下降和随机梯度下降法训练

## 总结

这里，我们介绍批量梯度下降法和随机梯度下降法，并且介绍如何在TensorFlow中实现。随机梯度下降法训练每次使用一次观测数据，而批量梯度下降法则是每次使用一组观测数据。

## 模型

在本脚本中，我们使用生成数据。我们将生成输入数据（满足正态分布（均值为1，标准差为0.1））。所有的目标都是10.0。使用模型来预测多个值，来最小化模型输出和值10.0之间的差异值。

## 提示

这里注意到TensorFlow擅长多维矩阵是非常重要的，因此我们能够很容易的实现批量梯度下降算法训练（正如这个脚本中介绍的在我们的输入中添加批量维度）


## 观察差异

这里我们在相同的计算图中画出随机和批量梯度下降法的损失函数。注意到随机梯度下降法的收敛性是不平滑的。虽然这个问题看起来是不太好，但是它能够帮助我们探索采样空间，并且不太容易陷入局部极小值。

![Stochastic and Batch](../images/06_Back_Propagation.png "Stochastic and Batch Loss")