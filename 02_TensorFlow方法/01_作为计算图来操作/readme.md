# 像计算图来操作

## 总结

在本脚本中，我们创建一个阵列，并将其喂给一个占位符。我们然后用它乘以一个常数。

## 计算图输出

在Tensorboard中观察计算图。

![One Operation](../images/01_Operations_on_a_Graph.png "An Operation on a Graph")