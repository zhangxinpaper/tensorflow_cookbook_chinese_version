## Ch 2: TensorFlow 方法
<kbd>
  <a href="02_TensorFlow_Way/01_Operations_as_a_Computational_Graph#operations-as-a-computational-graph">
    <img src="02_TensorFlow_Way/images/01_Operations_on_a_Graph.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="02_TensorFlow_Way/02_Layering_Nested_Operations#multiple-operations-on-a-computational-graph">
    <img src="02_TensorFlow_Way/images/02_Multiple_Operations.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="02_TensorFlow_Way/03_Working_with_Multiple_Layers#working-with-multiple-layers">
    <img src="02_TensorFlow_Way/images/03_Multiple_Layers.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="02_TensorFlow_Way/04_Implementing_Loss_Functions#implementing-loss-functions">
    <img src="02_TensorFlow_Way/images/04_loss_fun1.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="02_TensorFlow_Way/05_Implementing_Back_Propagation#implementing-back-propagation">
    <img src="02_TensorFlow_Way/images/04_loss_fun2.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="02_TensorFlow_Way/06_Working_with_Batch_and_Stochastic_Training#working-with-batch-and-stochastic-training">
    <img src="02_TensorFlow_Way/images/06_Back_Propagation.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="02_TensorFlow_Way/07_Combining_Everything_Together#combining-everything-together">
    <img src="02_TensorFlow_Way/images/07_Combing_Everything_Together.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="02_TensorFlow_Way/08_Evaluating_Models#evaluating-models">
    <img src="02_TensorFlow_Way/images/08_Evaluating_Models.png" align="center" height="45" width="90">
  </a>
</kbd>

当我们已经介绍了基本的TensorFlow的对象和方法之后，我们现在想构造TensorFlow算法的单元。我们开始介绍计算图、然后介绍损失函数和反向传播。最后我们创建一个简单的分类器，并且展示一个回归和分类算法的例子。

- 像计算图一样操作一次
  - 展示如何在计算图中创建一个操作，并且如何使用Tensorboard可视化这个操作
- 层迭代操作
  - 展示如何在计算图中进行多个操作，并且如何使用Tensorboard可视化这些操作
- 多层实现
  - 展示如何在计算图中进行多层操作，并且如何使用Tensorboard可视化这些操作
- 实现损失函数
  - 为了训练，必须设定一个评估标准。这就是损失函数。我们画出各种损失函数，并且分析他们优缺点。
- 实现反向传播
  - 这里我们展示如何使用损失函数来迭代处理数据和反向传播误差，来实现回归和分类操作。
- SGD和Batch SGD训练
  - 在Tensorflow中使用批量梯度下降法和随机梯度下降法是很容易的。我们实现这两个训练方法，并比较它们的优缺点
- 组合上面的操作
  - 我们组合上面的操作，并实现一个简单的分类器
- 评估模型
  - 模型的好坏依赖于评估标准。这里我们用两个例子1）回归问题（MSE）；2）分类问题（Cross Entropy）。