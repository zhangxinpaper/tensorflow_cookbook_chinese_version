# 实现多层计算

## 总结

在本脚本中，我们在一个向量上执行一个1维的移动平均计算。然后我们通过把输出乘以一个特定的矩阵，来创建一个定制的运算。

## 空间移动窗口层

我们创建一个层，该层实现一个空间滑动窗口平均计算。我们的窗口大小为2x2，步进为2(高度和宽度)。滤波器的值为0.25，因为我们要做2x2窗口的平均呀！
```
my_filter = tf.constant(0.25, shape=[2, 2, 1, 1])
my_strides = [1, 2, 2, 1]
mov_avg_layer= tf.nn.conv2d(x_data, my_filter, my_strides, padding='SAME', name='Moving_Avg_Window')
```

## 定制层

我们创建一个定制层，为sigmoid(Ax+b)，这里x是一个2x2的矩阵，并且A和b是2x2的矩阵。

```
output = sigmoid( input * A + b )
```

## 计算图输出

老规矩，用tensorboard来看计算图。pytorch使用tensorboard-pytorch的版本看的！

![Multiple Layers](../images/03_Multiple_Layers.png "Multiple Layers on a Graph")