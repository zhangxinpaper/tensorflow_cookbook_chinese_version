## Ch 9: 循环神经网络

<kbd>
  <a href="09_Recurrent_Neural_Networks/01_Introduction#introduction-to-rnns-in-tensorflow">
    <img src="09_Recurrent_Neural_Networks/images/01_RNN_Seq2Seq.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="09_Recurrent_Neural_Networks/02_Implementing_RNN_for_Spam_Prediction#implementing-an-rnn-for-spam-prediction">
    <img src="09_Recurrent_Neural_Networks/images/02_RNN_Spam_Acc_Loss.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="09_Recurrent_Neural_Networks/03_Implementing_LSTM#implementing-an-lstm-model">
    <img src="09_Recurrent_Neural_Networks/images/03_LSTM_Loss.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="09_Recurrent_Neural_Networks/04_Stacking_Multiple_LSTM_Layers#stacking-multiple-lstm-layers">
    <img src="09_Recurrent_Neural_Networks/images/04_MultipleRNN_Architecture.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="09_Recurrent_Neural_Networks/05_Creating_A_Sequence_To_Sequence_Model#creating-a-sequence-to-sequence-model-with-tensorflow-seq2seq">
    <img src="09_Recurrent_Neural_Networks/images/05_Seq2Seq_Loss.png" align="center" height="45" width="90">
  </a>
</kbd>
<kbd>
  <a href="09_Recurrent_Neural_Networks/06_Training_A_Siamese_Similarity_Measure#training-a-siamese-similarity-measure-rnns">
    <img src="09_Recurrent_Neural_Networks/images/06_Similarity_RNN.png" align="center" height="45" width="90">
  </a>
</kbd>

循环神经网络（RNN）与普通的神经网络相似，除了RNN有循环链接，货真以来与网络过去状态的循环。这使得RNN可以有效处理序列数据，而其它的网络则不能有效处理。我们使用LSTM网络，其是通用RNN问题的很好解决方法。然后我们展示用Tensorflow是多么容易的实现这些RNN神经网络。

1. 介绍
   - 我们介绍循环神经网络，并且介绍如何向RNN喂数据，以及如何预测固定尺寸的目标（多类/数值）或者其他的序列目标（Sequence to Sequence）。
2. 实现Spam预测的RNN模型
   - 在本例中，我们创建RNN模型，来提高我们的spam/ham SMS文本的预测能力。
3. 实现文本生成的LSTM模型
   - 我们介绍如何实现一二LSTM（Long Short Term Memory）RNN，用于握手语言生成。
4. 堆积多个LSTM层
   - 我们使用多层LSTM层，来提高我们的握手语言生成效果（字符级别的字典）。
5. 创建一个序列到序列的翻译模型（Seq2Seq）
   - 这里，我们使用TensorFlow的sequence-to-sequence来训练一个英语到德语的翻译模型。
6. 训练一个Siamese相似性测度
   - 这里，我们实现一个Siamese RNN，来预测地址的相似度，以及用于记录匹配。用RNN来做记录匹配也可以用于其它场景。就算我们没有目标类别的固定集合的条件下，我们依然可以使用已经训练好的模型来预测新地址的相似度。